package com.achjqz.api


import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.junit.Test

class CryptoTest {
    @Test
    fun aes256() {
        val time = getCurrentSecond().toString()
        val sign = "$time/new_bangumi/7276tools.imiku.me".sha256() + "." + Aes256.encrypt(time, "tools.imiku.me")
        println("time: $time, sign: $sign")
        val client = OkHttpClient()
        val request = Request.Builder().url("https://tools-api.imiku.me/new_bangumi/7276")
            .addHeader("Origin", "https://tools.imiku.me")
            .addHeader("Authsign", sign).post("".toRequestBody()).build()
        val res = client.newCall(request).execute()
        println("res: ${res.body!!.string()}")
    }


    @Test
    fun sha256() {
        println("1567909741/new_bangumitools.imiku.me".sha256())
    }
}