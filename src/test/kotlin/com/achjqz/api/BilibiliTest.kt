package com.achjqz.api

import com.achjqz.api.bilibili.BilibiliApi
import com.achjqz.api.bilibili.constant.BilibiliCategory
import com.achjqz.api.bilibili.constant.BilibiliSearchType
import org.junit.Test

class BilibiliTest {

    @Test
    fun enum() {
        println(BilibiliCategory.BANGUMI.id)
    }
    @Test
    fun main_api() {
        println(BilibiliApi.getTypeSearchResult(keyword = "四月"))
        println(BilibiliApi.getRecommendInfo())
        println(BilibiliApi.getMediaInfo(BilibiliCategory.BANGUMI))
        println(BilibiliApi.getSeasonInfo(1699))
        println(BilibiliApi.getSeasonPlayUrlInfo(1608327, 3386418))
    }

    @Test
    fun season_info() {
        println(BilibiliApi.getSeasonInfo(24053))
    }

    @Test
    fun media_info() {
        println(BilibiliApi.getMediaInfo(BilibiliCategory.CHINA))
    }

    @Test
    fun vip_movie() {
        println(BilibiliApi.getSeasonPlayUrlInfo(40961594, 88446766))
    }

    @Test
    fun type_search() {
        println(BilibiliApi.getTypeSearchResult(BilibiliSearchType.VIDEO, "哈利波特", 1, 24))
    }

    @Test
    fun normal() {
        println(BilibiliApi.getNormalVideoInfo(67391869))
        println(BilibiliApi.getNormalVideoPlayUrl(67391869, "120672225"))
    }

    @Test
    fun ep() {
        println(BilibiliApi.getEpVideoInfo(259769))
    }
}