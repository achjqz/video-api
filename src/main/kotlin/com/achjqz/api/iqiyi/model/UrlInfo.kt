package com.achjqz.api.iqiyi.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UrlInfo(
    val code: String,
    val data: Data
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        val vidl: List<Vidl> = listOf()
    ) {
        @JsonClass(generateAdapter = true)
        data class Vidl(
            val m3utx: String,
            val m3u: String,
            val screenSize: String
        )
    }
}