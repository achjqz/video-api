package com.achjqz.api.iqiyi.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class EpisodeInfo(
    val msg: String,
    val total: Int,
    val code: String,
    val data: List<Data>
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        val name: String,
        val order: Int,
        val pic: String,
        val vid: String,
        val tvQId: Long,
        val desc: String
    )
}