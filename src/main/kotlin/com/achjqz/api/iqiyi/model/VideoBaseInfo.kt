package com.achjqz.api.iqiyi.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VideoBaseInfo(
    val code: String, // A00000
    val data: Data
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        val vid: String = "", //d6e76eaffafb50550274bf2564a6a500
        val name: String, //做家务的男人之魏大勋父子花式沙发瘫 尤长靖吐槽汪苏泷爱花钱
        val description: String = "",
        val tvId: Long, //4000724600
        val imageUrl: String = "",
        val albumImageUrl: String = "",
        val albumName: String = "",
        val shortTitle: String = "", //第1期 魏大勋父子花式沙发瘫
        val publishDate: String,
        val albumId: Long = 0L, // 223514201
        val score: Double = 8.0,
        val categories: List<Category>
    ) {
        @JsonClass(generateAdapter = true)
        data class Category(
            val name: String, //内地
            val subName: String //地区
        )
    }
}