package com.achjqz.api.iqiyi

import com.achjqz.api.*
import com.achjqz.api.iqiyi.constant.IqiyiCategory
import com.achjqz.api.iqiyi.model.*
import java.net.URLEncoder

object IqiyiApi {
    private const val BASE_INFO_URL = "http://iface2.iqiyi.com/video/3.0/v_play?"
    private const val BASE_DETAIL_URL = "http://pcw-api.iqiyi.com/video/video/baseinfo/"
    private const val BASE_CACHE_URL = "http://cache.m.iqiyi.com"
    private const val BASE_SEARCH_URL = "http://search.video.iqiyi.com/o?if=html5&"
    private const val BASE_CATEGORY_URL = "http://data2.itv.ptqy.gitv.tv/itv/"
    private const val API_KEY = "/?apiKey=d4d389524eba5dc0a897a240a8fe2d2f"
    private const val VERSION_INFO = "chnList/1646/7.10.0.68631/1/"
    private const val QUERY_INFO =
        "app_k=20168006319fc9e201facfbd7c2278b7&app_v=8.9.5&platform_id=10&dev_os=8.0.1&dev_ua=Android&net_sts=1&qyid=9900077801666C&secure_p=GPhone&secure_v=1&dev_hw={}&app_t=0&h5_url="

    private const val QUERY_DL =
        "&uid=2343279496&type=m3u8&agenttype=21&ptid=02022001010000000000&p=ffw0WQm3HH2wfrHd4m3sy8y1tMWfnZNEaKVB2VhPW3ntSuxIREwGkKzhVCLu7dJMEVqj8b&vt=2&src=02022001010000000000&qd_v=1"

    fun getTvidFromH5Url(url: String): VideoIDInfo? {
        val u = BASE_INFO_URL + QUERY_INFO + url
        client.newCall(u.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            val adapter = moshi.adapter<VideoIDInfo>(VideoIDInfo::class.java)
            return adapter.fromJson(body)
        }
    }

    fun getDetailInfo(tvid: Long): VideoBaseInfo? {
        val url = BASE_DETAIL_URL + tvid
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            //println(body)
            val adapter = moshi.adapter<VideoBaseInfo>(VideoBaseInfo::class.java)
            return adapter.fromJson(body)
        }
    }

    fun getCategoryList(pagesize: Int = 60): CategoryList? {
        val url = BASE_CATEGORY_URL + VERSION_INFO + pagesize + API_KEY
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            val adapter = moshi.adapter<CategoryList>(CategoryList::class.java)
            return adapter.fromJson(body)
        }
    }

    fun getCategoryInfo(
        category: IqiyiCategory = IqiyiCategory.ANIME,
        page: Int = 1,
        pagesize: Int = 24
    ): CategoryInfo? {
        val url = BASE_CATEGORY_URL + "albumList/1646/${category.id}///$page/$pagesize" + API_KEY
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            //println(body)
            val adapter = moshi.adapter<CategoryInfo>(CategoryInfo::class.java)
            return adapter.fromJson(body)
        }
    }

    fun getEpisodeInfo(aid: Long): EpisodeInfo? {
        val url = BASE_CATEGORY_URL + "albumVideo/1646/$aid//0/1/120" + API_KEY
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            val adapter = moshi.adapter<EpisodeInfo>(EpisodeInfo::class.java)
            return adapter.fromJson(body)
        }
    }


    fun getPlayUrlParam(tvid: Long, vid: String): String {
        return "/tmts/$tvid/$vid/?tm=" + getCurrentSecond() + QUERY_DL
    }

    fun getPlayUrl(params: String, vf: String): UrlInfo? {
        val url = "$BASE_CACHE_URL$params&vf=$vf"
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            val adapter = moshi.adapter<UrlInfo>(UrlInfo::class.java)
            return adapter.fromJson(body)
        }
    }

    fun globalSearch(keyword: String, page: Int = 1, pagesize: Int = 21): SearchResult? {
        val url =
            BASE_SEARCH_URL + "pageNum=$page" + "&pageSize=$pagesize" + "&key=${URLEncoder.encode(keyword, "UTF-8")}"
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            debug {
                println(body)
            }
            val adapter = moshi.adapter<SearchResult>(SearchResult::class.java)
            return adapter.fromJson(body)
        }
    }
}