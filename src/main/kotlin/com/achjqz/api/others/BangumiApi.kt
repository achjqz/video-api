package com.achjqz.api.others

import com.achjqz.api.*
import com.achjqz.api.others.model.BangumiInfo
import com.achjqz.api.others.model.BangumiUrl
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody

object BangumiApi {
    private const val NEW_BANGUMI = "/new_bangumi"
    private fun getRequest(path: String): Request {
        val time = getCurrentSecond().toString()
        val sign = "$time$path$SECRET_KEY".sha256() + "." + Aes256.encrypt(time, SECRET_KEY)
        return Request.Builder().url(BASE_URL + path)
            .addHeader("Origin", ORIGIN_URL)
            .addHeader("Authsign", sign).post("".toRequestBody()).build()
    }

    fun getAllBangumi(): BangumiInfo? {
        client.newCall(getRequest(NEW_BANGUMI)).execute().use {
            val body = it.body!!.string()
            val adapter = moshi.adapter<BangumiInfo>(BangumiInfo::class.java)
            return adapter.fromJson(body)
        }
    }

    fun getBangumiUrl(id: Long): BangumiUrl? {
        client.newCall(getRequest("$NEW_BANGUMI/$id")).execute().use {
            val body = it.body!!.string()
            val adapter = moshi.adapter<BangumiUrl>(BangumiUrl::class.java)
            return adapter.fromJson(body)
        }
    }
}