package com.achjqz.api.others.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GarbageInfo(
    @Json(name = "data")
    val `data`: List<Data> = listOf(),
    @Json(name = "msg")
    val msg: String = ""
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        @Json(name = "name")
        val name: String = "",
        @Json(name = "type")
        val type: String = ""
    )
}