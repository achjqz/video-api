package com.achjqz.api.others.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BangumiInfo(
    @Json(name = "data")
    val `data`: List<List<Item>> = listOf(),
    @Json(name = "msg")
    val msg: String = ""

) {
    @JsonClass(generateAdapter = true)
    data class Item(
        val id: String,
        val title: String,
        val update: String,
        val poster: String
    )
}