package com.achjqz.api.others.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BangumiUrl(
    @Json(name = "data")
    val `data`: Data = Data(),
    @Json(name = "msg")
    val msg: String = ""
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        @Json(name = "id")
        val id: String = "",
        @Json(name = "list")
        val list: List<Url> = listOf(),
        @Json(name = "poster")
        val poster: String = "",
        @Json(name = "title")
        val title: String = "",
        @Json(name = "update")
        val update: String = ""
    ) {
        @JsonClass(generateAdapter = true)
        data class Url(
            @Json(name = "num")
            val num: Int = 0,
            @Json(name = "src")
            val src: String = "",
            @Json(name = "type")
            val type: String = ""
        )
    }
}