package com.achjqz.api.bilibili.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SeasonInfo(
    val code: Int, // 0
    val message: String, // success
    val result: Result? = null
) {
    @JsonClass(generateAdapter = true)
    data class Result(
        val cover: String, // http://i0.hdslb.com/bfs/bangumi/a92892921f3209f7784a954c37467c9869a1d4c1.png
        val episodes: List<Episode> = listOf(),
        val evaluate: String, // 位于东京西部的巨大“学园都市”，实施着超能力开发的特殊课程。学生们的能力被给予从“无能力Level 0”到“超能力Level 5”的六阶段评价。高中生上条当麻，由于寄宿在右手中的力量——只要是异能之力...
        val link: String, // http://www.bilibili.com/bangumi/media/md134912/
        @Json(name = "new_ep")
        val newEp: NewEp? = null,
        val publish: Publish,
        val rating: Rating = Rating(0, 8.0),
        @Json(name = "season_id")
        val seasonId: Long, // 25617
        @Json(name = "share_url")
        val shareUrl: String, // http://m.bilibili.com/bangumi/play/ss25617
        @Json(name = "square_cover")
        val squareCover: String, // http://i0.hdslb.com/bfs/bangumi/91b29251445f9b808e9c30f34019d3ba4f128d6d.jpg
        val stat: Stat,
        val title: String, // 魔法禁书目录 第三季
        val total: Int, // 0
        @Json(name = "type_name")
        val typeName: String, // "番剧"
        val areas: List<Area> = listOf(),
        val actor: Actor,
        val staff: Staff,
        val alias: String,
        val section: List<Section>
    ) {
        @JsonClass(generateAdapter = true)
        data class Section(
            @Json(name = "episode_id")
            val epId: Int,
            val episodes: List<Episode>,
            val title: String
        )
        @JsonClass(generateAdapter = true)
        data class Episode(
            val aid: Long, // 44389470
            val cid: Long, // 77725026
            val cover: String, // http://i0.hdslb.com/bfs/archive/c83ef2a961d8b53d6a30f03e8fb631ea4248fede.jpg
            @Json(name = "long_title")
            val longTitle: String, // 守护的理由
            @Json(name = "share_url")
            val shareUrl: String, // https://m.bilibili.com/bangumi/play/ep250453
            val title: String // 20
        )

        @JsonClass(generateAdapter = true)
        data class Rating(
            val count: Int, // 32318
            val score: Double // 7.8
        )

        @JsonClass(generateAdapter = true)
        data class NewEp(
            val desc: String, // 连载中, 每周五22:30更新
            @Json(name = "is_new")
            val isNew: Int, // 1
            val title: String // 20
        )

        @JsonClass(generateAdapter = true)
        data class Stat(
            val coins: Long, // 333230
            val danmakus: Int, // 729836
            val favorites: Int, // 2300833
            val reply: Int, // 324057
            val share: Int, // 18362
            val views: Int // 41392306
        )

        @JsonClass(generateAdapter = true)
        data class Publish(
            @Json(name = "is_finish")
            val isFinish: Int, // 0
            @Json(name = "is_started")
            val isStarted: Int, // 1
            @Json(name = "pub_time")
            val pubTime: String, // 2018-10-05 22:30:00
            @Json(name = "pub_time_show")
            val pubTimeShow: String, // 10月05日22:30
            @Json(name = "release_date_show")
            val releaseShow: String // 2019年1月18日上映
        )

        @JsonClass(generateAdapter = true)
        data class Area(
            val id: Int,
            val name: String
        )

        @JsonClass(generateAdapter = true)
        data class Actor(val info: String, val title: String)

        @JsonClass(generateAdapter = true)
        data class Staff(val info: String, val title: String)
    }
}

