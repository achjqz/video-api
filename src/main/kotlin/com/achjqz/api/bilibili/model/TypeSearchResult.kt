package com.achjqz.api.bilibili.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TypeSearchResult(
    val code: Int, // 0
    val `data`: Data? = null,
    val message: String // 0
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        val items: List<Item> = listOf(),
        val pages: Int = 1, // 1
        val total: Int = 1// 1
    ) {
        @JsonClass(generateAdapter = true)
        data class Item(
            val area: String = "未知", // 日本
            val cover: String = "", // https://i0.hdslb.com/bfs/bangumi/4d9f43eb3dba572797f8915f8f28efce9e58d756.jpg
            val cv: String = "", // 桐人（桐谷和人）：松冈祯丞亚丝娜（结城明日奈）：户松遥爱丽丝：茅野爱衣尤吉欧：岛崎信长赛鲁卡：前田佳织里罗妮耶·亚拉贝尔：近藤玲奈蒂洁·修特利尼：石原夏织神代凛子：小林沙苗菊冈诚二郎：森川智之莱欧斯·安提诺斯：岩濑周平温贝尔·吉泽克：木岛隆一索尔狄丽娜·塞路尔特：潘惠美沃罗·利凡玎：村田太志诗乃（朝田诗乃）：泽城美雪 强尼·布莱克（金本敦）：逢坂良太 西莉卡（绫野珪子）：日高里菜 莉兹贝特（筱崎里香）：高垣彩阳
            val label: String = "未知", // 小说改/热血/奇幻/战斗/励志
            val ptime: Int, // 1538841600
            val rating: Double = 8.0, // 9.2
            @Json(name = "season_id")
            val seasonId: Long = -1, // 25510
            @Json(name = "season_type")
            val seasonType: Int = 1, // 1
            @Json(name = "season_type_name")
            val seasonTypeName: String = "未知", // 番剧
            val staff: String = "", // 原作：川原砾原作插画 / 角色设计草案：abec导演：小野学助理导演：佐久间贵史角色设计：足立慎吾、铃木豪、西口智也总作画监督：铃木豪、西口智也动作作画监督：菅野芳弘、竹内哲也美术导演：小川友佳子、渡边佳人美术设定：森冈贤一、谷内优穗色彩设计：中野尚美CG导演：云藤隆太音响导演：岩浪美和效果：小山恭正音响制作：ソニルード音乐：梶浦由记制片：EGG FIRM、Straight Edge制作：A-1 Pictures
            val style: String = "", // 小说改/热血/奇幻/战斗/励志
            val title: String, // 刀剑神域 Alicization
            val uri: String, // https://www.bilibili.com/bangumi/play/ss25510/
            val vote: Int = 0 // 48497
        )
    }
}
