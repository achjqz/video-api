package com.achjqz.api.bilibili

import com.achjqz.api.*
import com.achjqz.api.bilibili.constant.BilibiliCategory
import com.achjqz.api.bilibili.constant.BilibiliRecommendType
import com.achjqz.api.bilibili.constant.BilibiliSearchType
import com.achjqz.api.bilibili.model.*
import java.net.URLEncoder

object BilibiliApi {
    // 全局搜索关键词
    private const val BASE_GLOBAL_SEARCH_URL = "http://app.bilibili.com/x/v2/search?"
    // 搜索某一类别
    private const val BASE_TYPE_SEARCH_URL = "http://app.bilibili.com/x/v2/search/type?"

    // 获取普通视频信息
    private const val BASE_NORMAL_VIEW_URL = "http://app.bilibili.com/x/v2/view?"
    // 获取番剧信息
    private const val BASE_SEASON_VIEW_URL = "http://api.bilibili.com/pgc/view/app/season?"

    // 获取普通视频url
    private const val BASE_NORMAL_PLAY_URL = "http://app.bilibili.com/v2/playurlproj?"
    // 获取番剧视频url 用proj版视频不分段
    private const val BASE_SEASON_PLAY_URL = "http://api.bilibili.com/pgc/player/api/playurlproj?"

    // 获取某一分类的推荐信息
    private const val BASE_VIDEO_RECOMMEND_URL = "http://api.bilibili.com/pgc/app/v2/page/cinema/"


    // 获取某一分类动漫的推荐信息
    private const val BASE_BANGUMI_RECOMMEND_URL = "http://api.bilibili.com/pgc/app/v2/page/bangumi/"


    // 获取某一分类的所有影片信息
    private const val BASE_MEDIA_URL = "http://bangumi.bilibili.com/media/api/search/result?"

    private const val APP_SECRET = "560c52ccd288fed045859ed18bffd973"
    private const val VIDEO_APP_SECRET = "aHRmhWMLkdeMuILqORnYZocwMBpMEOdt"
    private const val APP_KEY = "1d8b6e7d45233436"
    private const val VIDEO_APP_KEY = "iVGUTjsxvpLeuDCf"
    private const val BUVID = "XY17E581A55BB981B77345789B5E198A331CF"
    private const val ACCESS_KEY = "7bf0e1dc29abc9c4d11169fa26b7dbb1"


    private const val QUERY_APP_KEY = "&appkey=$APP_KEY"
    private const val QUERY_VIDEO_APP_KEY = "&appkey=$VIDEO_APP_KEY"
    private const val QUERY_KEYWORD = "keyword="
    private const val QUERY_AID = "aid="
    private const val QUERY_CID = "&cid="
    private const val QUERY_PLATFORM =
        "&mobi_app=android&platform=android&force_host=0&build=5442100&npcybs=0&fnval=16&mid=7464597&fnver=0&fourk=1&access_key=$ACCESS_KEY&actionkey=appkey&otype=json"
    private const val QUERY_TS = "&ts="
    private const val QUERY_SEARCH_PN = "&pn="
    private const val QUERY_SEARCH_PS = "&ps="
    private const val QUERY_BUVID = "&buvid=$BUVID"
    private const val QUERY_SIGN = "&sign="
    private const val QUERY_QN = "&qn=114"
    private const val QUERY_SEASON_ID = "season_id="
    private const val QUERY_EP_ID = "ep_id="
    private const val QUERY_SEASON_TYPE = "season_type="
    private const val QUERY_MEDIA_PAGE = "&page="
    private const val QUERY_MEDIA_PAGESIZE = "&pagesize="
    private const val QUERY_TYPE = "&type="


    /**
     * 根据关键词获取搜索url
     * @param keyword 搜索关键词
     * @param pn 当前分页页数
     * @param ps 每一页个数
     */
    private fun getGlobalSearchSearchUrl(keyword: String, pn: Int, ps: Int): String {
        val search = URLEncoder.encode(keyword, "UTF-8")
        val key =
            QUERY_KEYWORD + search + QUERY_APP_KEY + QUERY_PLATFORM + QUERY_SEARCH_PN + pn + QUERY_SEARCH_PS + ps + QUERY_TS + getCurrentSecond()
        return BASE_GLOBAL_SEARCH_URL + key + QUERY_SIGN + calculateSign(
            key,
            APP_SECRET
        )
    }

    /**
     * 搜索某一类关键词的url
     * @param type 搜索分类
     * @param keyword 搜索关键词
     * @param pn 当前分页页数
     * @param ps 每一页个数
     */
    private fun getTypeSearchUrl(type: String, keyword: String, pn: Int, ps: Int): String {
        val search = URLEncoder.encode(keyword, "UTF-8")
        val key =
            QUERY_KEYWORD + search + QUERY_APP_KEY + QUERY_PLATFORM + QUERY_SEARCH_PN + pn + QUERY_SEARCH_PS + ps + QUERY_TYPE + type + QUERY_TS + getCurrentSecond()
        return BASE_TYPE_SEARCH_URL + key + QUERY_SIGN + calculateSign(
            key,
            APP_SECRET
        )
    }

    /**
     * 根据aid获取普通视频信息, 包括cid
     * @param aid 视频的uid, 即av号
     */
    private fun getNormalInfoUrl(aid: Long): String {
        val key = QUERY_AID + aid + QUERY_APP_KEY + QUERY_PLATFORM + QUERY_TS + getCurrentSecond()
        return BASE_NORMAL_VIEW_URL + key + QUERY_SIGN + calculateSign(
            key,
            APP_SECRET
        )
    }


    /**
     * 根据season id获取番剧信息, 包括cid
     * @param id 视频的season_id
     */
    private fun getSeasonInfoUrl(id: Long): String {
        val query = QUERY_SEASON_ID + id + QUERY_APP_KEY + QUERY_PLATFORM + QUERY_TS + getCurrentSecond()
        return BASE_SEASON_VIEW_URL + query + QUERY_SIGN + calculateSign(
            query,
            APP_SECRET
        )
    }

    /**
     * 根据ep id获取番剧信息, 包括cid和season_id
     * @param id 视频的ep_id
     */
    private fun getEpInfoUrl(id: Long): String {
        val query = QUERY_EP_ID + id + QUERY_APP_KEY + QUERY_PLATFORM + QUERY_TS + getCurrentSecond()
        return BASE_SEASON_VIEW_URL + query + QUERY_SIGN + calculateSign(
            query,
            APP_SECRET
        )
    }

    /**
     * 根据aid, cid获取普通视频播放url
     * @param aid 视频的uid, 即av号
     * @param cid 视频的cid,
     * @see getNormalInfoUrl
     */
    private fun getNormalPlayUrl(aid: Long, cid: String): String {
        val key =
            QUERY_AID + aid + QUERY_CID + cid + QUERY_BUVID + QUERY_VIDEO_APP_KEY + QUERY_PLATFORM + QUERY_QN + QUERY_TS + getCurrentSecond()
        return BASE_NORMAL_PLAY_URL + key + QUERY_SIGN + calculateSign(
            key,
            VIDEO_APP_SECRET
        )
    }

    /**
     * 根据aid, cid获取番剧播放url
     * @param aid 视频的uid, 即av号
     * @param cid 视频的cid,
     * @see getSeasonInfoUrl
     */
    private fun getSeasonPlayUrl(aid: Long, cid: Long): String {
        val key =
            QUERY_AID + aid + QUERY_CID + cid + QUERY_BUVID + QUERY_APP_KEY + QUERY_PLATFORM + QUERY_QN + QUERY_TS + getCurrentSecond()
        return BASE_SEASON_PLAY_URL + key + QUERY_SIGN + calculateSign(
            key,
            APP_SECRET
        )
    }

    /**
     * 根据分类获取视频列表
     * @param category 分类信息
     * @param page 当前页数
     * @param pagesize 每页个数
     */
    private fun getMediaUrl(category: String, page: Int, pagesize: Int): String {
        val query =
            QUERY_SEASON_TYPE + category + QUERY_APP_KEY + QUERY_PLATFORM + QUERY_MEDIA_PAGE + page + QUERY_MEDIA_PAGESIZE + pagesize + QUERY_TS + getCurrentSecond()
        return BASE_MEDIA_URL + query + QUERY_SIGN + calculateSign(
            query,
            APP_SECRET
        )
    }


    /**
     * 获取视频某一分类推荐信息
     * @param category 分类信息
     */
    private fun getRecommendVideoUrl(category: String): String {
        val query = QUERY_APP_KEY.substring(1) + QUERY_PLATFORM + QUERY_TS + getCurrentSecond()
        return BASE_VIDEO_RECOMMEND_URL + category + query + QUERY_SIGN + calculateSign(
            query,
            APP_SECRET
        )
    }

    /**
     * 获取番剧某一分类推荐信息
     * @param category 分类信息
     */
    private fun getRecommendBangumiUrl(category: String): String {
        val query = QUERY_APP_KEY.substring(1) + QUERY_PLATFORM + QUERY_TS + getCurrentSecond()
        return BASE_BANGUMI_RECOMMEND_URL + category + query + QUERY_SIGN + calculateSign(
            query,
            APP_SECRET
        )
    }

    fun getNormalVideoPlayUrl(aid: Long, cid: String): SeasonUrl? {
        val url = getNormalPlayUrl(aid, cid)
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            val adapter = moshi.adapter<SeasonUrl>(SeasonUrl::class.java)
            return adapter.fromJson(body)
        }

    }

    fun getNormalVideoInfo(aid: Long): NormalVideoInfo? {
        val url = getNormalInfoUrl(aid)
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            val adapter = moshi.adapter<NormalVideoInfo>(NormalVideoInfo::class.java)
            return adapter.fromJson(body)
        }
    }

    fun getEpVideoInfo(ep: Long): SeasonInfo? {
        val url = getEpInfoUrl(ep)
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            val adapter = moshi.adapter<SeasonInfo>(SeasonInfo::class.java)
            return adapter.fromJson(body)
        }
    }

    /**
     * 根据分类获取视频列表
     * @param category 分类信息
     * @see BilibiliCategory
     * @param page 当前页数
     * @param pagesize 每页个数
     */
    fun getMediaInfo(
        category: BilibiliCategory = BilibiliCategory.BANGUMI,
        page: Int = 1,
        pagesize: Int = 21
    ): MediaInfo? {
        val url = getMediaUrl(category.id, page, pagesize)
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            val adapter = moshi.adapter<MediaInfo>(MediaInfo::class.java)
            return adapter.fromJson(body)
        }
    }

    /**
     * 搜索某一类关键词
     * @param type 搜索分类
     * @see BilibiliSearchType
     * @param keyword 搜索关键词
     * @param pn 当前分页页数
     * @param ps 每一页个数
     */
    fun getTypeSearchResult(
        type: BilibiliSearchType = BilibiliSearchType.BANGUMI,
        keyword: String,
        pn: Int = 1,
        ps: Int = 21
    ): TypeSearchResult? {
        val url = getTypeSearchUrl(type.id, keyword, pn, ps)
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            val adapter = moshi.adapter<TypeSearchResult>(TypeSearchResult::class.java)
            return adapter.fromJson(body)
        }
    }

    /**
     * 获取视频某一分类推荐信息
     * @param category 分类信息
     * @see BilibiliRecommendType
     */
    fun getRecommendInfo(category: BilibiliRecommendType = BilibiliRecommendType.TV): RecommendInfo? {
        val url = getRecommendVideoUrl(category.type)
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            val adapter = moshi.adapter<RecommendInfo>(RecommendInfo::class.java)
            return adapter.fromJson(body)
        }
    }

    /**
     * 根据season id获取番剧信息, 包括cid
     * @param id 视频的season_id
     */
    fun getSeasonInfo(id: Long): SeasonInfo? {
        val url = getSeasonInfoUrl(id)
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            debug {
                println(body)
            }
            val adapter = moshi.adapter<SeasonInfo>(SeasonInfo::class.java)
            return adapter.fromJson(body)
        }
    }

    /**
     * 根据aid, cid获取番剧播放url
     * @param aid 视频的uid, 即av号
     * @param cid 视频的cid,
     * @see getSeasonInfoUrl
     */
    fun getSeasonPlayUrlInfo(aid: Long, cid: Long): SeasonUrl? {
        val url = getSeasonPlayUrl(aid, cid)
        client.newCall(url.toSimpleRequest()).execute().use {
            val body = it.body?.string() ?: return null
            debug {
                println(body)
            }
            val adapter = moshi.adapter<SeasonUrl>(SeasonUrl::class.java)
            return adapter.fromJson(body)
        }
    }
}