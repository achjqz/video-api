package com.achjqz.api.bilibili.constant

enum class BilibiliRecommendType(val type: String) {
    MOVIE("movie?"),
    TV("tv?"),
    JP("jp?")
}