package com.achjqz.api.bilibili.constant

enum class BilibiliSearchType(val id: String) {

    // anime
    @Deprecated("not support")
    ANIME("1"),

    // 用户
    @Deprecated("not support")
    USER("2"),

    // 真直播
    @Deprecated("not support")
    LIVE("4"),
    // 直播
    @Deprecated("not support")
    LIVE_UNKNOWN("5"),

    // 文章
    @Deprecated("not support")
    ARTICLE("6"),
    // 番剧
    BANGUMI("7"),
    // 影视
    VIDEO("8")
}