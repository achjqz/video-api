package com.achjqz.api

import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.Request

import java.security.MessageDigest
import java.time.Instant

val client = OkHttpClient()
val moshi = Moshi.Builder().build()!!
fun String.toSimpleRequest(): Request = Request.Builder().url(this).get().build()


fun sortedParams(params: String): String {
    return params.split('&').sorted().joinToString(separator = "&")
}

fun String.sha256(): String {
    val bytes = this.toByteArray()
    val md = MessageDigest.getInstance("SHA-256")
    val digest = md.digest(bytes)
    return digest.fold("", { str, it -> str + "%02x".format(it) })
}

fun String.md5(): String {
    val md5Instance = MessageDigest.getInstance("MD5")
    return StringBuilder(32).apply {
        //优化过的 md5 字符串生成算法
        md5Instance.digest(toByteArray()).forEach {
            val value = it.toInt() and 0xFF
            val high = value / 16
            val low = value % 16
            append(if (high <= 9) '0' + high else 'a' - 10 + high)
            append(if (low <= 9) '0' + low else 'a' - 10 + low)
        }
    }.toString()
}

fun getCurrentSecond(): Long = Instant.now().epochSecond
/**
 * 签名算法为 "$排序后的参数字符串$appSecret".md5()
 */
fun calculateSign(query: String, appSecret: String) = (sortedParams(query) + appSecret).md5()

const val DEBUG = true
fun debug(func: () -> Unit) {
    if (DEBUG) {
        func()
    }
}